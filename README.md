# Asia Release Production

*Release repository of helm charts, values and manifest files of every services for Production deployments*



### Requirements

- Python 3

  ​		*Native to Mac OSX or if none, download https://www.python.org/downloads/mac-osx/*

- Helm

​		`brew install asdf`

​		`asdf plugin-add helm`

​		`asdf install helm 2.13.1`

​		`asdf global helm 2.13.1`

- Virtualenv

​	`virtualenv -p python3 venv`

​	`pip install -r requirements.txt`

​	`vi venv/bin/activate` and append `export PROJECTS_PATH=<your_projects_path>` to the end. PROJECTS_PATH is a path where all repositories will be cloned. 


### Procedure
1. Activate virtualenv 	`source venv/bin/activate`
---
2. Get the details in the ticket provided for the release. let's example this ticket: https://dev.azure.com/retailshelf/rs/_workitems/edit/24099. Make sure the ticket has **Business Approver**, **IT Approver**, **Prod Deployment Approver**. Also make sure **Risk and Impact** tab has all the risk information provided. 
---
3. If the repository was already added you can skip this step. Add the repository template  on `services.yaml`. Follow alphabetical order
 
  `repository-name: &referenceName` - specify the reference name this template in a camelCaseFormat
	
  `<<: *pointToReferenceTemplate` - point to the root template
	
  `repo-url` - url of the repository
	
  `chart-path` - path of chart in the repo`
	
  `chart-root` - If there's a root folder of chart-path specific to that service. Commonly specified on service level`
	
  `istio-inject` - If the service needs to have istio. Can be overridden on service level`
	
  `version.node` - Specify the hash path of the image if not defined as global.hash`

````yaml
backend-ph-pesonet-outward-batch: &backendPhPesonetOutwardBatch
  <<: *serviceTemplate
  repo-url: 'ssh://git@gitlab.ing.net:2222/ASIA/backend-ph-pesonet-outward-batch.git'
  chart-path: 'src/main/helm'
  istio-inject: false
````
---
4. add the service template on `services.yaml` and point it to repository template. Follow alphabetical order
```yaml
pesonetoutwardbatch:
    <<: *backendPhPesonetOutwardBatch
    chart-root: parent_root_of_chart
    version.node: pesonetoutward.hash
```
---
5. define services to deploy  in `deployment.yaml`

`service` - name of the service

`branch ` - branch to be pulled

`version ` - version hash of the service specified in the ticket 
	
`chart-only`   #For debugging only. Set true if you dont want to pull the repo of specific service everytime you run prepare-deploy.yaml

```yaml
- service:  pesonetoutwardbatch
    branch:  release/RELEASE.BE.2019.25.6-RC0
    version: 2.0.0.RELEASE
    chart-only: false 
```
---
6. add prod values of that services to `overrides` folder from `_old_helm_manifest` folder.  
---
7. run python prepare-deploy.py. 
   Arguments: 
   `--copy-charts` **true** or **false**. Default to true. If you dont want to pull repository branch everytime
   `--env` **PRODUCTION** or **MARKETING**. Default to production. If you want to deploy in production or marketing

`python prepare_deploy.py`

`python prepare_deploy.py --copy-charts=False`

`python prepare_deploy.py --env=MARKETING`

---
8. Double check the changes and make sure all the requested changes in the ticket are applied. Contact the release owner for verification of the changes if necessary
---
9.  Commit changes and request Merge Request. 
---
10.  Once approved, you can now deploy to EQX and/or TCX

   ### Todos
   1. solution to patching