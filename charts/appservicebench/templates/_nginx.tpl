{{- define "appservicebenchconfig.nginxconfig" -}}
events {}

http {
  variables_hash_bucket_size 128;

  server{
    listen 0.0.0.0:8080;
    root /home/www;
    charset UTF-8;

    # Security Headers
    add_header X-Frame-Options "deny";
    add_header Access-Control-Allow-Origin null;
    add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;
    add_header X-Content-Type-Options "nosniff";
    add_header X-XSS-Protection "1; mode=block";
    # add_header Content-Security-Policy "default-src 'self'";
    add_header Referrer-Policy "same-origin";
    add_header Feature-Policy "sync-xhr 'self'";
    add_header Service-Worker-Allowed /;

    # Remove server version.
    server_tokens off;

    # Compression settings
    gzip on; # enable gzip
    gzip_vary on; # cache compressed and regular files
    gzip_min_length 10240; # min length in bytes before compression kicks in.
    gzip_proxied expired no-cache no-store private auth;
    gzip_types text/css text/javascript text/plain image/svg+xml application/xml font/woff font/woff2 image/png image/jpg;

    location / {
      types {
        font/woff                   woff;
        font/woff2                  woff2;
        text/javascript             js;
        text/css                    css;
        image/svg+xml               svg;
        image/jpg                   jpg;
        image/png                   png;
        application/manifest+json   webmanifest;
        application/pdf             pdf;
        application/json            json;
      }
      default_type "text/html";
      # try_files ensures nginx can handle routing in single page applications.
      try_files $uri $uri/ /app-servicebench/index.html;
      proxy_pass_header Server;
    }

    location /api {
      # Default to localhost. For local development, point the route to the
      # desired environment. If unset, you will always get 502s when making API
      # calls.
      proxy_pass https://localhost:8082;
    }

    location = /app-servicebench/elka-dashboard-analytics {
      return  301 {{ .Values.appservicebenchconfig.elkaDashboardAnalyticsUrl }};
    }
    
    # route 404s back to the home page.
    error_page 404 =200 /app-servicebench/index.html;
  }

  # Used to espace the word $state in elkaDashboardAnalyticsUrl
  geo $state {
    default "$state";
  }
}
{{- end -}}