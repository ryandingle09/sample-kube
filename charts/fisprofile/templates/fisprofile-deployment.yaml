apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "fisprofile.name" .}}
spec:
  replicas: {{ $.Values.fisprofile.replicas }}
  revisionHistoryLimit: {{ $.Values.global.revisionHistoryLimit }}
  selector:
    matchLabels:
      app: {{ template "fisprofile.name" .}}
      country: {{ .Values.fisprofile.country}}
  template:
    metadata:
      labels:
        app: {{ template "fisprofile.name" .}}
        country: {{ .Values.fisprofile.country}}
    spec:
      serviceAccountName: {{ .Values.fisprofile.serviceAccountName }}
      containers:
      - name: {{ template "fisprofile.name" .}}
        image: {{ .Values.global.repo}}/{{ .Values.fisprofile.image}}:{{ .Values.global.hash }}
        imagePullPolicy: {{ .Values.global.imagePullPolicy }}
        volumeMounts:
        {{- range $val := .Values.global.volumeMounts }}
        - name: {{ $val.name }}
          mountPath: {{ $val.mountPath }}
        {{-  end}}
        resources:
          requests:
            cpu: {{ .Values.fisprofile.resources.requests.cpu }}
            memory: {{ .Values.fisprofile.resources.requests.memory }}
          limits:
            cpu: {{ .Values.fisprofile.resources.limits.cpu }}
            memory: {{ .Values.fisprofile.resources.limits.memory }}
        readinessProbe:
          failureThreshold: {{ .Values.fisprofile.readinessProbe.failureThreshold }}
          httpGet:
            path: {{ .Values.fisprofile.readinessProbe.path }}
            port: {{ .Values.fisprofile.readinessProbe.port }}
            scheme: HTTP
          initialDelaySeconds: {{ .Values.fisprofile.readinessProbe.initialDelaySeconds }}
          periodSeconds: {{ .Values.fisprofile.readinessProbe.periodSeconds }}
          successThreshold: {{ .Values.fisprofile.readinessProbe.successThreshold }}
          timeoutSeconds: {{ .Values.fisprofile.readinessProbe.timeoutSeconds }}
        env:
        - name: CREDENTIALS_SOURCE
          value: {{ .Values.fisprofile.env.credentialsSource | quote }}
        - name: SECURITY_CONF_ENVIRONMENT
          value: {{ .Values.global.env.securityConfEnvironment | quote }}
        - name: server.ssl.enabled
          value: {{ .Values.global.env.serverSslEnabled | quote }}
        - name: spring.datasource.hikari.maximumPoolSize
          value: {{ .Values.global.env.hikariMaximumPoolSize | quote}}
        - name: COUNTRY_CODE
          value: {{ .Values.fisprofile.countryCode | quote}}
        - name: PROFILE_CORE_DBHOST
          value: {{ .Values.fisprofile.env.profileCoreDbHost | quote }}
        - name: PROFILE_CORE_DBPORT
          value: {{ .Values.fisprofile.env.profileCoreDbPort | quote }}
        - name: PROFILE_CORE_READONLYACCESS
          value: {{ .Values.fisprofile.env.profileCoreReadOnlyAccess | quote }}
        - name: PROFILE_CORE_HOSTPOOLMAXACTIVE
          value: {{ .Values.fisprofile.env.profileCoreHostPoolMaxActive | quote }}
        - name: PROXY_USER_ENABLED
          value: {{ .Values.fisprofile.env.proxyUserEnabled | quote }}
        - name: TLS_KEYSTORE_FILE
          value: {{ .Values.fisprofile.env.tlsKeystoreFile | quote }}
        - name: TLS_KEYSTORE_PASSWORD
          value: {{ .Values.fisprofile.env.tlsKeystorePassword | quote }}
        - name: TLS_TRUSTORE_FILE
          value: {{ .Values.global.env.tlsTruststoreFile | quote }}
        - name: TLS_TRUSTSTORE_PASSWORD
          value: {{ .Values.global.env.tlsTruststorePassword | quote }}
        - name: TLS_KEY_ALIAS
          value: {{ .Values.fisprofile.env.tlsKeyAlias | quote }}
        - name: TLS_KEY_PASSWORD
          value: {{ .Values.fisprofile.env.tlsKeyPassword | quote }}
        - name: PROFILE_READ_ONLY_ACCESS
          value: {{ .Values.fisprofile.env.profileReadOnlyAccess | quote }}
        ports:
        {{- range $val := .Values.global.ports }}
        - containerPort: {{ $val.port }}
        {{- end}}
      volumes:
      - name: certs-dir
        emptyDir: {}
      initContainers:
      - name: {{ .Values.global.certsContainer.name }}
        image: {{ .Values.global.repo}}/certificate-fetcher:{{ .Values.global.certsContainer.hash }}
        imagePullPolicy: {{ .Values.global.certsContainer.imagePullPolicy}}
        command: ["/bin/bash","-c","python3 app.py && ./convert-cert.sh"]
        env:
        - name: VAULT_URL
          value: {{ .Values.global.certsContainer.vaultUrl | quote }}
        - name: CN
          value: {{ .Values.fisprofile.cnName | quote }}
        - name: OU
          value: {{ .Values.fisprofile.ouName | quote }}
        - name: VAULT_ROLE
          value: {{ .Values.fisprofile.vaultRoleName | quote }}
        volumeMounts:
        {{- range $val := .Values.global.volumeMounts }}
        - name: {{ $val.name }}
          mountPath: {{ $val.mountPath }}
        {{-  end}}
      imagePullSecrets:
      - name: regsecret