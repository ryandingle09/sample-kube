apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "kycorchestrator.name" .}}
spec:
  replicas: {{ $.Values.kycorchestrator.replicas }}
  revisionHistoryLimit: {{ $.Values.global.revisionHistoryLimit }}
  selector:
    matchLabels:
      app: {{ template "kycorchestrator.name" .}}
      country: {{ .Values.global.country}}
  template:
    metadata:
      labels:
        app: {{ template "kycorchestrator.name" .}}
        country: {{ .Values.global.country}}
    spec:
      serviceAccountName: {{ .Values.global.serviceAccountName }}
      containers:
      - name: {{ template "kycorchestrator.name" .}}
        image: {{ .Values.global.repo}}/{{ .Values.kycorchestrator.image}}:{{ .Values.global.hash }}
        imagePullPolicy: {{ .Values.global.imagePullPolicy }}
        volumeMounts:
        {{- range $val := .Values.global.volumeMounts }}
        - name: {{ $val.name }}
          mountPath: {{ $val.mountPath }}
        {{-  end}}
        resources:
          requests:
            cpu: {{ .Values.kycorchestrator.resources.requests.cpu }}
            memory: {{ .Values.kycorchestrator.resources.requests.memory }}
          limits:
            cpu: {{ .Values.kycorchestrator.resources.limits.cpu }}
            memory: {{ .Values.kycorchestrator.resources.limits.memory }}
        readinessProbe:
          failureThreshold: {{ .Values.kycorchestrator.readinessProbe.failureThreshold }}
          httpGet:
            path: {{ .Values.kycorchestrator.readinessProbe.path }}
            port: {{ .Values.kycorchestrator.readinessProbe.port }}
            scheme: HTTP
          initialDelaySeconds: {{ .Values.kycorchestrator.readinessProbe.initialDelaySeconds }}
          periodSeconds: {{ .Values.kycorchestrator.readinessProbe.periodSeconds }}
          successThreshold: {{ .Values.kycorchestrator.readinessProbe.successThreshold }}
          timeoutSeconds: {{ .Values.kycorchestrator.readinessProbe.timeoutSeconds }}
        env:
        - name: CREDENTIALS_SOURCE
          value: {{ .Values.kycorchestrator.env.credentialsSource | quote }}
        - name: SECURITY_CONF_ENVIRONMENT
          value: {{ .Values.global.env.securityConfEnvironment | quote }}
        - name: server.ssl.enabled
          value: {{ .Values.global.env.serverSslEnabled | quote }}
        - name: ACCESS_CONTROL_ALLOW_ORIGIN
          value: {{ .Values.global.accessControlAllowOrigin | quote }}
        - name: COUNTRY_CODE
          value: {{ .Values.global.countryCode | quote}}
        - name: TLS_KEYSTORE_FILE
          value: {{ .Values.kycorchestrator.env.tlsKeystoreFile | quote }}
        - name: TLS_KEYSTORE_PASSWORD
          value: {{ .Values.kycorchestrator.env.tlsKeystorePassword | quote }}
        - name: TLS_TRUSTORE_FILE
          value: {{ .Values.global.env.tlsTruststoreFile | quote }}
        - name: TLS_TRUSTSTORE_PASSWORD
          value: {{ .Values.global.env.tlsTruststorePassword | quote }}
        - name: TLS_KEY_ALIAS
          value: {{ .Values.kycorchestrator.env.tlsKeyAlias | quote }}
        - name: TLS_KEY_PASSWORD
          value: {{ .Values.kycorchestrator.env.tlsKeyPassword | quote }}
        - name: MEANS_USER
          value: {{ .Values.kycorchestrator.env.meansUser | quote }}
        - name: MEANS_PASSWORD
          value: {{ .Values.kycorchestrator.env.meansPassword | quote }}
        - name: REQUESTOR
          value: {{ .Values.kycorchestrator.env.requestor | quote }}
        ports:
        {{- range $val := .Values.global.ports }}
        - containerPort: {{ $val.port }}
        {{- end}}
      volumes:
      - name: certs-dir
        emptyDir: {}
      initContainers:
      - name: {{ .Values.global.certsContainer.name }}
        image: {{ .Values.global.repo}}/certificate-fetcher:{{ .Values.global.certsContainer.hash }}
        imagePullPolicy: {{ .Values.global.certsContainer.imagePullPolicy}}
        command: ["/bin/bash","-c","python3 app.py && ./convert-cert.sh"]
        env:
        - name: VAULT_URL
          value: {{ .Values.global.certsContainer.vaultUrl | quote }}
        - name: CN
          value: {{ .Values.kycorchestrator.cnName | quote }}
        - name: OU
          value: {{ .Values.kycorchestrator.ouName | quote }}
        volumeMounts:
        {{- range $val := .Values.global.volumeMounts }}
        - name: {{ $val.name }}
          mountPath: {{ $val.mountPath }}
        {{-  end}}
      imagePullSecrets:
      - name: regsecret