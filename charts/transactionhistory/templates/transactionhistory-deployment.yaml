apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "transactionhistory.name" .}}
spec:
  replicas: {{ .Values.transactionhistory.replicas }}
  revisionHistoryLimit: {{ .Values.global.revisionHistoryLimit }}
  selector:
    matchLabels:
      app: {{ template "transactionhistory.name" .}}
      country: {{ .Values.global.country}}
  template:
    metadata:
      labels:
        app: {{ template "transactionhistory.name" .}}
        country: {{ .Values.global.country}}
    spec:
      serviceAccountName: {{ .Values.global.serviceAccountName }}
      containers:
      - name: {{ template "transactionhistory.name" .}}
        image: {{ .Values.global.repo}}/{{ .Values.transactionhistory.image}}:{{ .Values.global.hash }}
        imagePullPolicy: {{ .Values.global.imagePullPolicy }}
        volumeMounts:
        {{- range $val := .Values.global.volumeMounts }}
        - name: {{ $val.name }}
          mountPath: {{ $val.mountPath }}
        {{-  end}}
        resources:
          requests:
            cpu: {{ .Values.transactionhistory.resources.requests.cpu }}
            memory: {{ .Values.transactionhistory.resources.requests.memory }}
          limits:
            cpu: {{ .Values.transactionhistory.resources.limits.cpu }}
            memory: {{ .Values.transactionhistory.resources.limits.memory }}
        readinessProbe:
          failureThreshold: {{ .Values.transactionhistory.readinessProbe.failureThreshold }}
          httpGet:
            path: {{ .Values.transactionhistory.readinessProbe.path }}
            port: {{ .Values.transactionhistory.readinessProbe.port }}
            scheme: HTTP
          initialDelaySeconds: {{ .Values.transactionhistory.readinessProbe.initialDelaySeconds }}
          periodSeconds: {{ .Values.transactionhistory.readinessProbe.periodSeconds }}
          successThreshold: {{ .Values.transactionhistory.readinessProbe.successThreshold }}
          timeoutSeconds: {{ .Values.transactionhistory.readinessProbe.timeoutSeconds }}
        env:
        - name: CREDENTIALS_SOURCE
          value: {{ .Values.global.env.vaultCredentialsSource | quote }}
        - name: SECURITY_CONF_ENVIRONMENT
          value: {{ .Values.global.env.securityConfEnvironment | quote }}
        - name: spring.datasource.hikari.minimumIdle
          value: {{ .Values.global.env.hikariMinimumIdle | quote}}
        - name: spring.datasource.hikari.maxLifetime
          value: {{ .Values.global.env.hikariMaxLifetime | quote}}
        - name: spring.datasource.hikari.maximumPoolSize
          value: {{ .Values.global.env.hikariMaximumPoolSize | quote }}
        - name: server.ssl.enabled
          value: {{ .Values.global.env.serverSslEnabled | quote }}
        - name: ACCESS_CONTROL_ALLOW_ORIGIN
          value: {{ .Values.global.accessControlAllowOrigin | quote }}
        - name: COUNTRY_CODE
          value: {{ .Values.global.countryCode | quote}}
        - name: TLS_TRUSTORE_FILE
          value: {{ .Values.global.env.tlsTruststoreFile | quote }}
        - name: TLS_TRUSTSTORE_PASSWORD
          value: {{ .Values.global.env.tlsTruststorePassword | quote }}
        - name: TLS_KEYSTORE_FILE
          value: {{ .Values.transactionhistory.env.tlsKeystoreFile | quote }}
        - name: TLS_KEYSTORE_PASSWORD
          value: {{ .Values.transactionhistory.env.tlsKeystorePassword | quote }}
        - name: TLS_KEY_ALIAS
          value: {{ .Values.transactionhistory.env.tlsKeyAlias | quote }}
        - name: TLS_KEY_PASSWORD
          value: {{ .Values.transactionhistory.env.tlsKeyPassword | quote }}
        ports:
        {{- range $val := .Values.global.ports }}
        - containerPort: {{ $val.port }}
        {{- end}}
      volumes:
      - name: certs-dir
        emptyDir: {}
      initContainers:
      - name: {{ .Values.global.certsContainer.name }}
        image: {{ .Values.global.repo}}/certificate-fetcher:{{ .Values.global.certsContainer.hash }}
        imagePullPolicy: {{ .Values.global.certsContainer.imagePullPolicy}}
        command: ["/bin/bash","-c","python3 app.py && ./convert-cert.sh"]
        env:
        - name: VAULT_URL
          value: {{ .Values.global.certsContainer.vaultUrl | quote }}
        - name: CN
          value: {{ .Values.transactionhistory.cnName | quote }}
        - name: OU
          value: {{ .Values.transactionhistory.ouName | quote }}
        volumeMounts:
        {{- range $val := .Values.global.volumeMounts }}
        - name: {{ $val.name }}
          mountPath: {{ $val.mountPath }}
        {{-  end}}
      imagePullSecrets:
      - name: regsecret
