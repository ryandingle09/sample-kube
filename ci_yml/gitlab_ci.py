#!/usr/bin/env python3

import operator
import os
import sys
from ruamel.yaml import YAML


script_dir = os.path.dirname(os.path.abspath(__file__))


class ci_yaml:
    def __init__(self, services=[], env=''):
        self.yaml=YAML(typ='rt')
        self.yaml.allow_duplicate_keys = False
        self.yaml.preserve_quotes = True
        self.yaml.default_flow_style = False
        self.yaml.allow_unicode = True
        self.yaml.block_seq_indent = 2
        self.yaml.indent = 4
        self.yaml.width = 4096
        self.env = env
        self.service_list = services    
        # self.data_path = ''.join([script_dir, '/values_jobs.yaml'])
        self.data_path = os.path.join(script_dir, 'values_jobs.yaml')
        self.master_list_path = os.path.abspath(os.path.join(script_dir, '..', 'services.yaml'))

    
    def yaml_load(self, yaml_path):
        with open(yaml_path, 'r') as stream:
            return self.yaml.load(stream)


    def create_deploy_specific_jobs(self):
        data = self.yaml_load(self.data_path)
        services = self.yaml_load(self.master_list_path)
        job_list = []
        for prod in data['global'][self.env.lower()]:
            oc_apply_all = ''
            script_source = 'set -e; source ' + prod['source']
            oc_login = "oc login --context "+ prod['namespace'] +"/masters-"+ prod['cluster'] +"-prod-ichp-asia-ing-net:443/$OPENSHIFT_USER -u=$OPENSHIFT_USER -p=$OPENSHIFT_PASS -n=$OPENSHIFT_PROJECT $OPENSHIFT_CLUSTER_PATH"
            oc_apply_context = "oc apply --context " + prod['namespace'] + "/masters-"+prod['cluster']+"-prod-ichp-asia-ing-net:443/$OPENSHIFT_USER"

            for service in self.service_list:
                oc_apply = ''.join([oc_apply_context, ' -f manifests/', prod['name'] , '/', service, '.yaml'])
                oc_apply_all += oc_apply_context + ' -f manifests/' + prod['name'] + '/' + service + '.yaml;'
                deploy_base = '*DeploySpecific_' + prod['name']
                job_name = ''.join(['dsm', '-', prod['name'], '-', service])
                new_jobs = dict(job_name=job_name, deploybase=deploy_base, script=[{'script_login':oc_login,'script_source':script_source, 'script_deploycommand': oc_apply}])
                job_list.append(new_jobs)

            all_name = ''.join(['da', '-', prod['name']])
            all_base = '*DeployAllRelease'
            all_jobs = dict(job_name=all_name, deploybase=all_base, script=[{'script_login':oc_login,'script_source':script_source, 'script_deploycommand': oc_apply_all}])
            job_list.append(all_jobs)

            job_list.sort(key=operator.itemgetter('job_name'))
            data['global']['jobs'] = job_list
            job_list = data['global']['jobs']
                
        with open(self.data_path , 'w') as stream:
            self.yaml.dump(data, stream)


    def remove_old_ci_yaml(self):
        try:
            os.remove(os.path.abspath(os.path.join(script_dir, '..', '.gitlab-ci.yaml')))
        except:
            pass

    def execute(self):
        helm_cmd = 'helm template {0}/gitlab-ci-chart -f {0}/values_base.yaml -f {0}/values_jobs.yaml > {1}/.gitlab-ci.yml'.format(script_dir, os.path.abspath(os.path.join(script_dir, '..')))
        self.remove_old_ci_yaml()
        self.create_deploy_specific_jobs()
        os.system(helm_cmd)
