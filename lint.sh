#!/bin/bash

set -e

source /home/gitlab-runner/.env/prod-tcx.env

namespace="eqx_production eqx_marketing tcx_production tcx_marketing"

for n in $namespace; do
    for f in manifests/${n}/*.y*ml; do
        service=${f##*/}
        overrides_file="overrides/${n}/$service"
        echo $overrides_file
        echo $service
        if [[ -f ${overrides_file} ]]; then
            # helm lint charts/${service%.*} -f ${overrides_file}
            if [ "$(ls -A manifests/${n}/*.y*ml)" ] ; then
                kubeval "manifests/"${n}/$service --schema-location https://gitlab.ing.net --project-id 41960 --private-token $GITLAB_PRIVATE_TOKEN
            else
                echo "No Manifest found in $n"
            fi
            if [[ $? -ne 0 ]]; then
                exit 1
            fi
        else
            # helm lint charts/${service%.*}
            if [ "$(ls -A manifests/${n}/*.y*ml)" ] ; then
                kubeval "manifests/"${n}/$service --schema-location https://gitlab.ing.net --project-id 41960 --private-token $GITLAB_PRIVATE_TOKEN
            else
                echo "No Manifest found in $n"
            fi
            if [[ $? -ne 0 ]]; then
                exit 1
            fi
        fi
    done
done