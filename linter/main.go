package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"gopkg.in/matryer/try.v1"
)

var numberofthread int = 10
var gitlabtoken string = os.Getenv("GITLAB_PRIVATE_TOKEN")

func lint(queue chan string, thread int, done, killsig chan bool) {
	for true {
		select {
		case file := <-queue:
			err := try.Do(
				func(attempt int) (bool, error) {
					var err error
					out, err := exec.Command("bash", "-c", "kubeval "+file+" --schema-location https://gitlab.ing.net --project-id 41960 --private-token "+gitlabtoken).Output()
					fmt.Println("Using thread ID:", thread)
					if err != nil {
						fmt.Println("Retrying...", attempt)
						fmt.Println(string(out[:]))
					} else {
						fmt.Println(string(out[:]))
					}
					return attempt < 5, err
				})
			if err != nil {
				log.Fatal(err)
			}
			done <- true
		case <-killsig:
			fmt.Println("Closing thread ID:", thread)
			return
		}
	}
}

func main() {

	// set channels
	queue := make(chan string)
	done := make(chan bool)
	killsignal := make(chan bool)

	// get all manifest files
	cwd, _ := os.Getwd()
	files, _ := filepath.Glob(cwd + "/manifests/*/*.y*ml")

	// open 10 threads of lint goroutine
	for tr := 0; tr < numberofthread; tr++ {
		go lint(queue, tr, done, killsignal)
	}

	// queue all manifest files to lint goroutine
	for _, file := range files {
		go func(file string) {
			queue <- file
		}(file)
	}

	//check once done
	for c := 0; c < len(files); c++ {
		<-done
	}
	fmt.Print("Kube validation finished!\n\n")

	// close all threads
	close(killsignal)
	time.Sleep(2 * time.Second)

}
