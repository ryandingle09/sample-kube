---
# Source: massnotification/templates/massnotification-deployment.yaml
apiVersion: v1
kind: Service
metadata:
  name: massnotification
spec:
  ports:
    - port: 8080
      targetPort: 8080
      name: http
    - port: 8081
      targetPort: 8081
      name: http-management
    - port: 8443
      targetPort: 8443
      name: https
    - port: 8444
      targetPort: 8444
      name: https-management
  selector:
    app: massnotification
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  creationTimestamp: null
  name: massnotification
spec:
  replicas: 1
  revisionHistoryLimit: 1
  selector:
    matchLabels:
      app: massnotification
      country: asia
      version: v1
  strategy: {}
  template:
    metadata:
      annotations:
        sidecar.istio.io/status: '{"version":"fcf7c3eaafe1cced7e9af99af83fbe556a07aa912cc7fb059bf4d3abb2486828","initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-certs"],"imagePullSecrets":null}'
      creationTimestamp: null
      labels:
        app: massnotification
        country: asia
        version: v1
    spec:
      containers:
      - args:
        - -Djava.security.egd=file:/dev/./urandom
        - -XX:+UnlockExperimentalVMOptions
        - -XX:+UseCGroupMemoryLimitForHeap
        - -javaagent:/tmp/jmx_prometheus_javaagent-0.3.1.jar=5556:config.yaml
        - -XX:MaxRAMFraction=2
        - -jar
        - /app.jar
        command:
        - java
        env:
        - name: CREDENTIALS_SOURCE
          value: vault
        - name: SECURITY_CONF_ENVIRONMENT
          value: production
        - name: server.ssl.enabled
          value: "false"
        - name: HIKAR_MIN_IDLE
          value: "10"
        - name: HIKARI_MAX_LIFE_TIME
          value: "540000"
        - name: HIKARI_MAXIMUM_POOL_SIZE
          value: "25"
        - name: DATASOURCE_VAULT_USERNAME_KEY
          value: username
        - name: DATASOURCE_VAULT_PASSKEY
          value: password
        - name: DATASOURCE_VAULT_PATH
          value: kv/database
        - name: VAULT_ROLE
          value: read
        - name: ACCESS_CONTROL_ALLOW_ORIGIN
          value: https://api.ing.com.ph
        - name: COUNTRY_CODE
          value: ASIA
        - name: BATCH_PAGE_SIZE
          value: "1000"
        - name: BATCH_CHUNK_SIZE
          value: "1"
        - name: REQUESTOR
          value: System
        - name: BATCH_COUNTRY_CODE
          value: PH
        - name: SMS_COST
          value: "0.85"
        - name: DEFAULT_FILTER_ADV
          value: '{"filters": [{ "code": "CIF", "operator": "IS", "value": "NOT NULL",
            "valueType": "Option" }, { "code": "SAV_ACCT_CLOSURE_STAT", "operator":
            "!=", "value": "CLOSED", "valueType": "Option" }]}'
        - name: DEFAULT_FILTER_PROM
          value: '{"filters": [{ "code": "CIF", "operator": "IS", "value": "NOT NULL",
            "valueType": "Option" }, { "code": "SAV_ACCT_CLOSURE_STAT", "operator":
            "!=", "value": "CLOSED", "valueType": "Option" }, { "code": "SAV_ACCT_NUM",
            "operator": "IS", "value": "NOT NULL", "valueType": "Option" }]}'
        - name: PROFILER_ENABLED
          value: "false"
        - name: WHITELIST_NOTIF_REQUEST_INFO_GROUPLIST
          value: DB,TEST
        - name: WHITELIST_NOTIF_REQUEST_INFO_SMS_CHANNEL
          value: SMS
        - name: WHITELIST_NOTIF_REQUEST_INFO_SMS_MESSAGE
          value: This is a test message for sms channel [first_name]
        - name: WHITELIST_NOTIF_REQUEST_INFO_SMS_SUBJECT
        - name: WHITELIST_NOTIF_REQUEST_INFO_SMS_TOPIC
        - name: WHITELIST_NOTIF_REQUEST_INFO_PUSH_CHANNEL
          value: PUSH_NOTIF
        - name: WHITELIST_NOTIF_REQUEST_INFO_PUSH_MESSAGE
          value: Message for push channel [first_name]
        - name: WHITELIST_NOTIF_REQUEST_INFO_PUSH_SUBJECT
          value: Whitelist Test
        - name: WHITELIST_NOTIF_REQUEST_INFO_PUSH_TOPIC
        - name: WHITELIST_NOTIF_REQUEST_INFO_INAPP_CHANNEL
        - name: WHITELIST_NOTIF_REQUEST_INFO_INAPP_MESSAGE
        - name: WHITELIST_NOTIF_REQUEST_INFO_INAPP_SUBJECT
        - name: WHITELIST_NOTIF_REQUEST_INFO_INAPP_TOPIC
        - name: WHITELIST_NOTIF_REQUEST_INFO_PROMOTIONAL
          value: "false"
        - name: WHITELIST_CHUNK_SIZE
          value: "1000"
        - name: WHITELISTED_CUSTOMERS
          valueFrom:
            configMapKeyRef:
              key: whitelisted-customers
              name: whitelisted-config
        - name: RANDOM_CIFS_SIZE
          value: "100"
        image: asia.docker.ing.net/backend/mass-notification:1.2.4
        imagePullPolicy: Always
        lifecycle:
          preStop:
            httpGet:
              path: mass-notification/abortstalejob
              port: 8080
              scheme: HTTP
        name: massnotification
        ports:
        - containerPort: 8080
        - containerPort: 8443
        - containerPort: 8444
        - containerPort: 8081
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /actuator/health
            port: 8081
            scheme: HTTP
          initialDelaySeconds: 20
          periodSeconds: 5
          successThreshold: 1
          timeoutSeconds: 1
        resources:
          limits:
            cpu: "4"
            memory: 4Gi
          requests:
            cpu: 400m
            memory: 2Gi
        volumeMounts:
        - mountPath: /certs
          name: certs-dir
        - mountPath: /config
          name: config-dir
      - args:
        - proxy
        - sidecar
        - --domain
        - $(POD_NAMESPACE).svc.cluster.local
        - --configPath
        - /etc/istio/proxy
        - --binaryPath
        - /usr/local/bin/envoy
        - --serviceCluster
        - massnotification.$(POD_NAMESPACE)
        - --drainDuration
        - 45s
        - --parentShutdownDuration
        - 1m0s
        - --discoveryAddress
        - istio-pilot.istio-system:15010
        - --zipkinAddress
        - zipkin.istio-system:9411
        - --proxyLogLevel=error
        - --connectTimeout
        - 30s
        - --proxyAdminPort
        - "15000"
        - --concurrency
        - "2"
        - --controlPlaneAuthPolicy
        - NONE
        - --statusPort
        - "15020"
        - --applicationPorts
        - 8080,8443,8444,8081
        env:
        - name: POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: INSTANCE_IP
          valueFrom:
            fieldRef:
              fieldPath: status.podIP
        - name: ISTIO_META_POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: ISTIO_META_CONFIG_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: ISTIO_META_INTERCEPTION_MODE
          value: REDIRECT
        - name: ISTIO_METAJSON_LABELS
          value: |
            {"app":"massnotification","country":"asia","version":"v1"}
        image: registry-all.docker.ing.net/istio/proxyv2:1.1.9
        imagePullPolicy: IfNotPresent
        name: istio-proxy
        ports:
        - containerPort: 15090
          name: http-envoy-prom
          protocol: TCP
        readinessProbe:
          failureThreshold: 30
          httpGet:
            path: /healthz/ready
            port: 15020
          initialDelaySeconds: 1
          periodSeconds: 2
        resources:
          limits:
            cpu: "2"
            memory: 1Gi
          requests:
            cpu: 100m
            memory: 128Mi
        securityContext:
          privileged: true
          readOnlyRootFilesystem: true
          runAsUser: 1337
        volumeMounts:
        - mountPath: /etc/istio/proxy
          name: istio-envoy
        - mountPath: /etc/certs/
          name: istio-certs
          readOnly: true
      imagePullSecrets:
      - name: regsecret
      initContainers:
      - command:
        - /bin/bash
        - -c
        - python3 app.py fetchbootstrap && ./convert-cert.sh
        env:
        - name: VAULT_URL
          value: https://vault:8200
        - name: CN
          value: massnotification
        - name: OU
          value: Massnotification
        image: asia.docker.ing.net/backend/certificate-fetcher:74cceea9
        imagePullPolicy: Always
        name: certs-container
        resources: {}
        volumeMounts:
        - mountPath: /certs
          name: certs-dir
        - mountPath: /config
          name: config-dir
      - args:
        - -p
        - "15001"
        - -u
        - "1337"
        - -m
        - REDIRECT
        - -i
        - 172.0.0.0/8
        - -x
        - ""
        - -b
        - 8080,8443,8444,8081
        - -d
        - "15020"
        image: registry-all.docker.ing.net/istio/proxy_init:1.1.9
        imagePullPolicy: IfNotPresent
        name: istio-init
        resources:
          limits:
            cpu: 100m
            memory: 50Mi
          requests:
            cpu: 10m
            memory: 10Mi
        securityContext:
          capabilities:
            add:
            - NET_ADMIN
          privileged: true
          runAsNonRoot: false
          runAsUser: 0
      serviceAccountName: vault-auth
      volumes:
      - emptyDir: {}
        name: certs-dir
      - emptyDir: {}
        name: config-dir
      - emptyDir:
          medium: Memory
        name: istio-envoy
      - name: istio-certs
        secret:
          optional: true
          secretName: istio.vault-auth
status: {}
---
# Source: massnotification/templates/massnotification-service.yaml
apiVersion: v1
kind: Service
metadata:
  name: massnotification
spec:
  ports:
  - port: 8080
    targetPort: 8080
    name: http
  - port: 8081
    targetPort: 8081
    name: http-health
  - port: 8443
    targetPort: 8443
    name: https
  - port: 8444
    targetPort: 8444
    name: http-management
  - port: 5556
    targetPort: 5556
    name: prometheus
  selector:
    app: massnotification
---
