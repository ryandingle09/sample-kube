apiVersion: v1
kind: Service
metadata:
  name: pushnotification
spec:
  ports:
  - name: http
    port: 8080
    targetPort: 8080
  - name: http-health
    port: 8081
    targetPort: 8081
  - name: https
    port: 8443
    targetPort: 8443
  - name: http-management
    port: 8444
    targetPort: 8444
  - name: prometheus
    port: 5556
    targetPort: 5556
  selector:
    app: pushnotification
---
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  name: pushnotification
spec:
  minReadySeconds: 20
  replicas: 3
  revisionHistoryLimit: 1
  selector:
    matchLabels:
      app: pushnotification
      country: asia
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 0
    type: RollingUpdate
  template:
    metadata:
      annotations:
        sidecar.istio.io/status: '{"version":"fcf7c3eaafe1cced7e9af99af83fbe556a07aa912cc7fb059bf4d3abb2486828","initContainers":["istio-init"],"containers":["istio-proxy"],"volumes":["istio-envoy","istio-certs"],"imagePullSecrets":null}'
      creationTimestamp: null
      labels:
        app: pushnotification
        country: asia
    spec:
      containers:
      - args:
        - -Dhttps.proxyHost=10.15.2.66
        - -Dhttps.proxyPort=8080
        - -Xbootclasspath/p:/alpn.jar
        - -Dhttp.nonProxyHosts='localhost|127.0.0.1|172.*.*.*|vault|*vault*'
        - -Djava.security.egd=file:/dev/./urandom
        - -XX:+UnlockExperimentalVMOptions
        - -XX:+UseCGroupMemoryLimitForHeap
        - -jar
        - /app.jar
        command:
        - java
        env:
        - name: CREDENTIALS_SOURCE
          value: vault
        - name: KAFKA_SETTING
          value: "false"
        - name: BOOTSTRAP_SERVER
          value: ingasia-kafka.southeastasia.cloudapp.azure.com:9092
        - name: HIKARI_MAXIMUM_POOL_SIZE
          value: "25"
        - name: SECURITY_CONF_ENVIRONMENT
          value: production
        - name: server.ssl.enabled
          value: "false"
        - name: ACCESS_CONTROL_ALLOW_ORIGIN
          value: https://api.ing.com.ph
        - name: COUNTRY_CODE
          value: ASIA
        - name: TLS_KEYSTORE_FILE
          value: file:/certs/pushnotification.jks
        - name: TLS_TRUSTORE_FILE
          value: file:/certs/ca.jks
        - name: TLS_KEY_ALIAS
          value: pushnotification
        - name: PUSH_NOTIFICATION_PROD_ENABLED
          value: "true"
        - name: PUSH_KEYSTORE
          value: /certs/ING_MPP_Prod_APNS.p12
        - name: IOS_DEFAULT_TOPIC
          value: com.ing.asia.mobile
        - name: DATASOURCE_VAULT_USERNAME_KEY
          value: username
        - name: DATASOURCE_VAULT_PASSKEY
          value: password
        - name: DATASOURCE_VAULT_PATH
          value: database/creds/readwrite_access
        - name: VAULT_ROLE
          value: pushnotification
        - name: FIREBASE_SERVICE_ACCOUNT_PATH
          value: /certs/firebase.json
        - name: FIREBASE_DATABASE_URL
          value: https://asia-android.firebaseio.com
        - name: BATCH_PARTITION
          value: "100"
        image: asia.docker.ing.net/backend/push-notification:1.0.3
        imagePullPolicy: IfNotPresent
        name: pushnotification
        ports:
        - containerPort: 8080
        - containerPort: 8081
        - containerPort: 8443
        - containerPort: 8444
        - containerPort: 5556
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /actuator/health
            port: 8081
            scheme: HTTP
          initialDelaySeconds: 20
          periodSeconds: 5
          successThreshold: 1
          timeoutSeconds: 1
        resources:
          limits:
            cpu: "4"
            memory: 4Gi
          requests:
            cpu: 400m
            memory: 1Gi
        volumeMounts:
        - mountPath: /certs
          name: certs-dir
        - mountPath: /config
          name: config-dir
      - args:
        - proxy
        - sidecar
        - --domain
        - $(POD_NAMESPACE).svc.cluster.local
        - --configPath
        - /etc/istio/proxy
        - --binaryPath
        - /usr/local/bin/envoy
        - --serviceCluster
        - pushnotification.$(POD_NAMESPACE)
        - --drainDuration
        - 45s
        - --parentShutdownDuration
        - 1m0s
        - --discoveryAddress
        - istio-pilot.istio-system:15010
        - --zipkinAddress
        - zipkin.istio-system:9411
        - --proxyLogLevel=error
        - --connectTimeout
        - 30s
        - --proxyAdminPort
        - "15000"
        - --concurrency
        - "2"
        - --controlPlaneAuthPolicy
        - NONE
        - --statusPort
        - "15020"
        - --applicationPorts
        - 8080,8081,8443,8444,5556
        env:
        - name: POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: INSTANCE_IP
          valueFrom:
            fieldRef:
              fieldPath: status.podIP
        - name: ISTIO_META_POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: ISTIO_META_CONFIG_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: ISTIO_META_INTERCEPTION_MODE
          value: REDIRECT
        - name: ISTIO_METAJSON_LABELS
          value: |
            {"app":"pushnotification","country":"asia"}
        image: registry-all.docker.ing.net/istio/proxyv2:1.1.9
        imagePullPolicy: IfNotPresent
        name: istio-proxy
        ports:
        - containerPort: 15090
          name: http-envoy-prom
          protocol: TCP
        readinessProbe:
          failureThreshold: 30
          httpGet:
            path: /healthz/ready
            port: 15020
          initialDelaySeconds: 1
          periodSeconds: 2
        resources:
          limits:
            cpu: "2"
            memory: 1Gi
          requests:
            cpu: 100m
            memory: 128Mi
        securityContext:
          privileged: true
          readOnlyRootFilesystem: true
          runAsUser: 1337
        volumeMounts:
        - mountPath: /etc/istio/proxy
          name: istio-envoy
        - mountPath: /etc/certs/
          name: istio-certs
          readOnly: true
      imagePullSecrets:
      - name: regsecret
      initContainers:
      - command:
        - /bin/bash
        - -c
        - python3 app.py notificationconsumer fetchbootstrap pushnotification && ./convert-cert.sh
          notificationconsumer
        env:
        - name: VAULT_URL
          value: https://vault:8200
        - name: VAULT_ROLE
          value: pushnotification
        - name: CN
          value: pushnotification
        - name: OU
          value: PushNotification
        image: asia.docker.ing.net/backend/certificate-fetcher:1.0.3
        imagePullPolicy: IfNotPresent
        name: certs-container
        resources: {}
        volumeMounts:
        - mountPath: /certs
          name: certs-dir
        - mountPath: /config
          name: config-dir
      - args:
        - -p
        - "15001"
        - -u
        - "1337"
        - -m
        - REDIRECT
        - -i
        - 172.0.0.0/8
        - -x
        - ""
        - -b
        - 8080,8081,8443,8444,5556
        - -d
        - "15020"
        image: registry-all.docker.ing.net/istio/proxy_init:1.1.9
        imagePullPolicy: IfNotPresent
        name: istio-init
        resources:
          limits:
            cpu: 100m
            memory: 50Mi
          requests:
            cpu: 10m
            memory: 10Mi
        securityContext:
          capabilities:
            add:
            - NET_ADMIN
          privileged: true
          runAsNonRoot: false
          runAsUser: 0
      serviceAccountName: pushnotification-vault-auth
      volumes:
      - emptyDir: {}
        name: certs-dir
      - emptyDir: {}
        name: config-dir
      - emptyDir:
          medium: Memory
        name: istio-envoy
      - name: istio-certs
        secret:
          optional: true
          secretName: istio.pushnotification-vault-auth
status: {}
---
