---
# Source: servicebench/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: servicebench-configmap
data:
  nginx.conf: |
    events {}

    http {
      variables_hash_bucket_size 128;

      server{
        listen 0.0.0.0:8080;
        root /home/www;

        # Security Headers
        add_header X-Frame-Options "deny";
        add_header Access-Control-Allow-Origin null;
        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;
        add_header X-Content-Type-Options "nosniff";
        add_header X-XSS-Protection "1; mode=block";
        add_header Content-Security-Policy "default-src 'self' 'unsafe-inline'; font-src 'self' data:; img-src 'self' data:; frame-src blob:; object-src blob:";
        add_header Referrer-Policy "same-origin";
        add_header Feature-Policy "sync-xhr 'self'";
        add_header Service-Worker-Allowed /;

        # Remove server version.
        server_tokens off;

        # Compression settings
        gzip on; # enable gzip
        gzip_vary on; # cache compressed and regular files
        gzip_min_length 10240; # min length in bytes before compression kicks in.
        gzip_proxied expired no-cache no-store private auth;
        gzip_types text/css text/javascript text/plain image/svg+xml application/xml font/woff font/woff2 image/png image/jpg;

        location / {
          types {
            font/woff                   woff;
            font/woff2                  woff2;
            text/javascript             js;
            text/css                    css;
            image/svg+xml               svg;
            image/jpg                   jpg;
            image/png                   png;
            application/manifest+json   webmanifest;
            application/pdf             pdf;
            application/json            json;
          }
          default_type "text/html";
          # try_files ensures nginx can handle routing in single page applications.
          try_files $uri /servicebench/index.html;
          proxy_pass_header Server;
        }

        location = /servicebench/elka-dashboard-analytics {
          return  301 https://alpt006.ic.ing.net:5601/app/kibana#/dashboard/04de48c0-4fa7-11e9-b32f-41fb1b53cf47?embed=true&_g=(refreshInterval:(display:'5+seconds',pause:!f,section:1,value:5000),time:(from:now-90d,mode:quick,to:now))&_a=(description:'CCA+Dashboard+Analytics+Customer+Specific+Production',filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:'7eada850-1adf-11e9-a819-8dabcf017dc3',key:cif,negate:!f,params:(query:'$arg_cif',type:phrase),type:phrase,value:'$arg_cif'),query:(match:(cif:(query:'$arg_cif',type:phrase))))),fullScreenMode:!f,options:(darkTheme:!f,hidePanelTitles:!f,useMargins:!t),panels:!((embeddableConfig:(),gridData:(h:23,i:'1',w:48,x:0,y:0),id:cf37cc60-4f96-11e9-b32f-41fb1b53cf47,panelIndex:'1',type:search,version:'6.3.0'),(embeddableConfig:(),gridData:(h:21,i:'2',w:24,x:24,y:23),id:e6ccaba0-4fa2-11e9-b32f-41fb1b53cf47,panelIndex:'2',type:search,version:'6.3.0'),(embeddableConfig:(),gridData:(h:21,i:'3',w:24,x:0,y:23),id:'1652b260-4fa4-11e9-b32f-41fb1b53cf47',panelIndex:'3',type:search,version:'6.3.0'),(embeddableConfig:(),gridData:(h:16,i:'4',w:24,x:0,y:44),id:b28342b0-4f9c-11e9-b32f-41fb1b53cf47,panelIndex:'4',type:search,version:'6.3.0'),(embeddableConfig:(),gridData:(h:16,i:'5',w:24,x:24,y:44),id:'80242720-4fa2-11e9-b32f-41fb1b53cf47',panelIndex:'5',type:search,version:'6.3.0'),(embeddableConfig:(),gridData:(h:19,i:'6',w:24,x:0,y:60),id:b2aee360-4f98-11e9-b32f-41fb1b53cf47,panelIndex:'6',type:search,version:'6.3.0'),(embeddableConfig:(),gridData:(h:19,i:'7',w:24,x:24,y:60),id:'54a8e520-4f9f-11e9-b32f-41fb1b53cf47',panelIndex:'7',type:search,version:'6.3.0')),query:(language:lucene,query:''),timeRestore:!t,title:'%5BELKA%5D%5BApp%5D%5BCCA%5D+Dashboard+Analytics+Customer+Specific+Production',viewMode:view);
        }

        # route 404s back to the home page.
        error_page 404 =200 /index.html;
      }

      # Used to espace the word $state in elkaDashboardAnalyticsUrl
      geo $state {
        default "$state";
      }
    }

---
# Source: servicebench/templates/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: servicebenchweb
spec:
  ports:
  - port: 8080
    targetPort: 8080
    name: http
  # service type will always be of type ClusterIP unless specified
  type: ClusterIP
  selector:
    app: servicebenchweb
---
# Source: servicebench/templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: servicebenchweb
spec:
  replicas: 3
  revisionHistoryLimit: 1
  selector:
    matchLabels:
      app: servicebenchweb
      country: asia
  template:
    metadata:
      labels:
        app: servicebenchweb
        country: asia
    spec:
      containers:
      - name: servicebenchweb
        # take note of the repo location - this should follow the agreed upon format
        image: asia.docker.ing.net/frontend/service-bench:21cdc520
        imagePullPolicy: Always
        resources:
          requests:
            cpu: 200m
            memory: 32Mi
          limits:
            cpu: 1
            memory: 128Mi
        ports:
        - containerPort: 8080
        volumeMounts:
        - name: servicebench-configmap-volume
          mountPath: /etc/nginx/
      volumes:
        - name: servicebench-configmap-volume
          configMap:
            name: servicebench-configmap
      imagePullSecrets:
      - name: regsecret

