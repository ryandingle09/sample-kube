#!/usr/bin/env python3

import os
import subprocess
# import sys

import click

from collections import OrderedDict
from ruamel.yaml import YAML
from retry_decorator import *
from ci_yml.gitlab_ci import ci_yaml


class PrepareDeploy:
    def __init__(self, copy_charts, create_manifests, datacenter, env):
        self.__yaml = YAML()
        self.__yaml.allow_duplicate_keys = True
        self.__yaml.indent(offset=2, sequence=4)
        self.__yaml.preserve_quotes = True
        self.__yaml.Representer.add_representer(OrderedDict, self.__yaml.Representer.represent_dict)
        self.__yaml.width = 4096

        self.__projects_path = os.getenv('PROJECTS_PATH')
        self.__repo_root_dir = os.getcwd()

        self.__deployments_yaml = {}
        self.__services_yaml = {}

        self.__copy_charts = copy_charts
        self.__create_manifests = create_manifests
        self.__env = env.upper()
        self.__datacenter = datacenter.upper()

    @staticmethod
    def __check_override_exists(override_file):
        print('Checking if values override file ' + override_file + ' exists...')
        if os.path.exists(override_file) and os.path.isfile(override_file):
            print('Values override file ' + override_file + ' exists.')
        return os.path.exists(override_file) and os.path.isfile(override_file)

    @staticmethod
    def __check_repo_exists(repo_git_path):
        print('Checking if ' + repo_git_path + ' exists...')
        if os.path.exists(repo_git_path) and os.path.isdir(repo_git_path):
            print('Repository exists in PROJECTS_PATH.')
        else:
            print('Repository does not exist in PROJECTS_PATH.')
        return os.path.exists(repo_git_path) and os.path.isdir(repo_git_path)

    @staticmethod
    def __get_repo_dir_name(repo_url):
        repo_dir_name = repo_url.split('/')[len(repo_url.split('/')) - 1]
        if repo_dir_name[-4:] == '.git':
            repo_dir_name = repo_dir_name[:-4]
        return repo_dir_name

    @staticmethod
    @retry(Exception, tries = 5)
    def __set_branch(deployment, service, repo_path, repo_exists):
        if repo_exists:
            print('Fetching latest changes from ' + service['repo-url'] + '...')
            subprocess.run(['git', '-C', repo_path, 'reset', 'HEAD', '--hard'], check=True)
            subprocess.run(['git', '-C', repo_path, 'clean', '-df'], check=True)
            subprocess.run(['git', '-C', repo_path, 'checkout', '-B', 'master'], check=True)
            subprocess.run(['git', '-C', repo_path, 'gc', '--prune=now'], check=True)
            subprocess.run(['git', '-C', repo_path, 'fetch', 'origin'], check=True)

            print('Checking out ' + deployment['branch'] + ' branch...')
            subprocess.run(['git', '-C', repo_path, 'branch', '-D', deployment['branch']])
            subprocess.run(
                ['git', '-C', repo_path, 'checkout', '-b', deployment['branch'], 'origin/' + deployment['branch']],
                check=True
            )
        else:
            print('Cloning ' + service['repo-url'] + ' repository into ' + repo_path + '...')
            subprocess.run(['git', 'clone', service['repo-url'], repo_path], check=True)

            print('Checking out ' + deployment['branch'] + ' branch...')
            subprocess.run(
                ['git', '-C', repo_path, 'checkout', '-b', deployment['branch'], 'origin/' + deployment['branch']],
                check=True
            )

    @staticmethod
    def __terminate(error_message, exit_code):
        print(error_message)
        exit(exit_code)

    def __check_parameters(self):
        print('Checking parameters...')
        if type(self.__copy_charts) is not bool:
            self.__terminate('Invalid value passed for --copy-charts parameter. Please provide a boolean value.', 1)

        if type(self.__create_manifests) is not bool:
            self.__terminate('Invalid value passed for --create-manifests parameter. ' +
                             'Please provide a boolean value.', 1)

        if not (self.__env == 'PRODUCTION' or self.__env == 'MARKETING' or self.__env == 'ALL'):
            self.__terminate('--env parameter ' + self.__env +
                             ' is invalid. Please specify a valid environment/project.', 1)
        print('All parameters are valid.')

    def __check_projects_path(self):
        print('Checking PROJECTS_PATH...')
        if not self.__projects_path:
            self.__terminate('Environment variable PROJECTS_PATH is not set.', 10)
        elif not (os.path.exists(self.__projects_path) and os.path.isdir(self.__projects_path)):
            self.__terminate('PROJECTS_PATH ' + self.__projects_path + ' does not exist or is not a directory.', 10)
        print('PROJECTS_PATH is set to ' + self.__projects_path + '.')

    def __check_services(self):
        print('Loading deployments.yaml...')
        with open(os.path.join(self.__repo_root_dir, 'deployments.yaml'), 'r') as deployments_file:
            self.__deployments_yaml = self.__yaml.load(deployments_file)
        print('deployments.yaml loaded successfully.')

        print('Loading services.yaml...')
        with open(os.path.join(self.__repo_root_dir, 'services.yaml'), 'r') as services_file:
            self.__services_yaml = self.__yaml.load(services_file)
        print('services.yaml loaded successfully.')

        print('Checking services for deployment...')
        for deployment in self.__deployments_yaml['deployments']:
            if not deployment['service'] in self.__services_yaml['services']:
                self.__terminate(deployment['service'] + ' is not a valid service.', 20)
        print('All services for deployment are valid.')

    def __copy_chart(self, deployment, service, repo_path, charts_dir):
        service_chart_dir = os.path.join(repo_path, service['chart-root'], service['chart-path'], deployment['service'])
        service_chart_yaml = os.path.join(service_chart_dir, 'Chart.yaml')
        if not (os.path.exists(service_chart_dir) and os.path.isdir(service_chart_dir)):
            self.__terminate('Chart directory ' + service_chart_dir +
                             ' is invalid. Either Chart does not exist or is not a directory.', 30)
        elif not (os.path.exists(service_chart_yaml) and os.path.isfile(service_chart_yaml)):
            self.__terminate('Chart is invalid. Chart.yaml file does not exist or is not a valid file.', 30)

        print('Copying Chart from ' + service_chart_dir + ' into ' + charts_dir + '...')
        subprocess.run(['cp', '-R', service_chart_dir, charts_dir], check=True)
        print(deployment['service'] + ' Chart was copied successfully.')

    def __create_manifest(self, deployment, charts_dir, manifests_dir, overrides_dir):
        service_chart = os.path.join(charts_dir, deployment['service'])
        manifest_filename = deployment['service'] + '.yaml'
        service_override_file = os.path.join(overrides_dir, manifest_filename)
        universal_override_file = os.path.join(overrides_dir, 'universal.yaml')

        if self.__check_override_exists(service_override_file):
            with open(os.path.join(manifests_dir, manifest_filename), 'w+') as manifest_file:
                subprocess.run(
                    ['helm', 'template', service_chart, '-f', universal_override_file, '-f', service_override_file],
                    check=True,
                    stdout=manifest_file,
                    text=True
                )
        else:
            with open(os.path.join(manifests_dir, manifest_filename), 'w+') as manifest_file:
                subprocess.run(
                    ['helm', 'template', service_chart, '-f', universal_override_file],
                    check=True,
                    stdout=manifest_file,
                    text=True
                )
        print('Manifest file ' + os.path.join(manifests_dir, manifest_filename) + ' has been created.')

    def __process_deployments(self):
        for deployment in self.__deployments_yaml['deployments']:
            print('----------------------------------------------------------------------')
            print('Processing ' + deployment['service'] + '...')
            print('----------------------------------------------------------------------')
            service = self.__services_yaml['services'][deployment['service']]
            repo_dir_name = self.__get_repo_dir_name(service['repo-url'])
            repo_path = os.path.join(self.__projects_path, repo_dir_name)
            charts_dir = os.path.join(self.__repo_root_dir, 'charts')
            manifest_filename = deployment['service'] + '.yaml'
            eqx_prod_manifests_dir = os.path.join(self.__repo_root_dir, 'manifests', 'eqx_production')
            eqx_prod_overrides_dir = os.path.join(self.__repo_root_dir, 'overrides', 'eqx_production')
            eqx_mark_manifests_dir = os.path.join(self.__repo_root_dir, 'manifests', 'eqx_marketing')
            eqx_mark_overrides_dir = os.path.join(self.__repo_root_dir, 'overrides', 'eqx_marketing')

            tcx_prod_manifests_dir = os.path.join(self.__repo_root_dir, 'manifests', 'tcx_production')
            tcx_prod_overrides_dir = os.path.join(self.__repo_root_dir, 'overrides', 'tcx_production')
            tcx_mark_manifests_dir = os.path.join(self.__repo_root_dir, 'manifests', 'tcx_marketing')
            tcx_mark_overrides_dir = os.path.join(self.__repo_root_dir, 'overrides', 'tcx_marketing')
            
            if self.__copy_charts:
                repo_exists = self.__check_repo_exists(os.path.join(repo_path, '.git'))
                self.__set_branch(deployment, service, repo_path, repo_exists)
                self.__copy_chart(deployment, service, repo_path, charts_dir)
            # print('----------------------------------------------------------------------')

            if self.__create_manifests:
                if self.__env == 'PRODUCTION':
                    if not deployment['chart-only']:
                        print('Updating version of ' + deployment['service'] + ' for EQX Production...')
                        self.__update_versions(deployment, service, eqx_prod_overrides_dir)
                    print('Creating ' + deployment['service'] + ' manifest for EQX Production...')
                    self.__create_manifest(deployment, charts_dir, eqx_prod_manifests_dir, eqx_prod_overrides_dir)
                    # print('----------------------------------------------------------------------')
                    if self.__services_yaml['services'][deployment['service']]['istio-inject']:
                        self.__inject_istio(deployment, eqx_prod_manifests_dir, manifest_filename)
                    # if self.__services_yaml['cronjobs'][deployment['service']]:
                    # cronjobs = []
                    # cronjobs[[cronjobs if cronjobs == deployment['service'] else None for cron in self.__services_yaml['cronjobs'] ]]

                    if not deployment['chart-only']:
                        print('Updating version of ' + deployment['service'] + ' for TCX Production...')
                        self.__update_versions(deployment, service, tcx_prod_overrides_dir)
                    print('Creating ' + deployment['service'] + ' manifest for TCX Production...')
                    self.__create_manifest(deployment, charts_dir, tcx_prod_manifests_dir, tcx_prod_overrides_dir)
                    # print('----------------------------------------------------------------------')
                    if self.__services_yaml['services'][deployment['service']]['istio-inject']:
                        self.__inject_istio(deployment, tcx_prod_manifests_dir, manifest_filename)
                
                if self.__env == 'MARKETING':
                    if not deployment['chart-only']:
                        print('Updating version of ' + deployment['service'] + ' for EQX Marketing...')
                        self.__update_versions(deployment, service, eqx_mark_overrides_dir)
                    print('Creating ' + deployment['service'] + ' manifest for EQX Marketing...')
                    self.__create_manifest(deployment, charts_dir, eqx_mark_manifests_dir, eqx_mark_overrides_dir)

                    if not deployment['chart-only']:
                        print('Updating version of ' + deployment['service'] + ' for TCX Marketing...')
                        self.__update_versions(deployment, service, tcx_mark_overrides_dir)
                    print('Creating ' + deployment['service'] + ' manifest for TCX Marketing...')
                    self.__create_manifest(deployment, charts_dir, tcx_mark_manifests_dir, tcx_mark_overrides_dir)

    def __inject_istio(self, deployment, manifests_dir, manifest_filename):
        # for deployment in self.__deployments_yaml['deployments']:
        if self.__services_yaml['services'][deployment['service']]['istio-inject']:
            print('----------------------------------------------------------------------')
            print('Injecting istio to ' + deployment['service'] + '...')
            
            inject_config = 'istio/inject-config.yaml'
            mesh_config = 'istio/mesh-config.yaml'
            manifest_injected_filename = deployment['service'] + '-injected.yaml'

            with open(os.path.join(manifests_dir, manifest_injected_filename), 'w+') as manifest_injected:
                subprocess.run(
                    ['istioctl', 'kube-inject', '--injectConfigFile', inject_config, '--meshConfigFile', mesh_config, '-f', manifests_dir + '/' + manifest_filename], 
                    check=True,
                    stdout=manifest_injected)
            os.rename(os.path.join(manifests_dir, manifest_injected_filename), os.path.join(manifests_dir, manifest_filename))
            print('Finished injecting istio to manifest ' + os.path.join(manifests_dir, manifest_filename) + '.')
            print('----------------------------------------------------------------------')

    def __update_version_override_exists(self, override_file, override_yaml_nodes, version_nodes):
        with open(override_file, 'r') as original_override_file:
            override_yaml = self.__yaml.load(original_override_file)
        # print('Original Override YAML:')
        # print(override_yaml)

        if override_yaml is None:
            override_yaml = OrderedDict([(version_nodes[0], override_yaml_nodes[0])])
            # self.__yaml.dump(override_yaml, sys.stdout)
        else:
            override_yaml_copy = override_yaml
            for i in range(len(version_nodes)):
                if version_nodes[i] not in override_yaml_copy:
                    if i == 0:
                        override_yaml[version_nodes[i]] = override_yaml_nodes[i]
                        # self.__yaml.dump(override_yaml, sys.stdout)
                        break
                    elif i == (len(version_nodes) - 1):
                        override_yaml_copy[version_nodes[i]] = override_yaml_nodes[i - 1][version_nodes[i]]
                        # self.__yaml.dump(override_yaml, sys.stdout)
                        break
                    else:
                        override_yaml_copy[version_nodes[i]] = override_yaml_nodes[i]
                        # self.__yaml.dump(override_yaml, sys.stdout)
                        break
                else:
                    if i == (len(version_nodes) - 1):
                        override_yaml_copy[version_nodes[i]] = override_yaml_nodes[i - 1][version_nodes[i]]
                        # self.__yaml.dump(override_yaml, sys.stdout)
                        break
                    else:
                        override_yaml_copy = override_yaml_copy[version_nodes[i]]

        # self.__yaml.dump(override_yaml, sys.stdout)
        with open(override_file, 'w+') as new_override_file:
            self.__yaml.dump(override_yaml, new_override_file)

    def __update_version_override_missing(self, override_file, override_yaml_nodes, version_nodes):
        override_yaml = OrderedDict([(version_nodes[0], override_yaml_nodes[0])])

        # self.__yaml.dump(override_yaml, sys.stdout)
        with open(override_file, 'w+') as new_override_file:
            self.__yaml.dump(override_yaml, new_override_file)

    def __update_versions(self, deployment, service, overrides_dir):
        override_filename = deployment['service'] + '.yaml'
        override_file = os.path.join(overrides_dir, override_filename)
        version_nodes = service['version-node'].split('.')
        # print('Version Nodes:')
        # print(version_nodes)

        override_yaml_nodes = [None]*(len(version_nodes) - 1)
        for i in range(len(override_yaml_nodes)):
            override_yaml_nodes[i] = OrderedDict([(version_nodes[i + 1], None)])
        for i in range((len(override_yaml_nodes) - 1), -1, -1):
            if i == (len(override_yaml_nodes) - 1):
                override_yaml_nodes[i][version_nodes[i + 1]] = deployment['version']
            else:
                override_yaml_nodes[i][version_nodes[i + 1]] = override_yaml_nodes[i + 1]
        # print('Override YAML Nodes:')
        # print(override_yaml_nodes)

        if self.__check_override_exists(override_file):
            self.__update_version_override_exists(override_file, override_yaml_nodes, version_nodes)
        else:
            self.__update_version_override_missing(override_file, override_yaml_nodes, version_nodes)
        print('Version of ' + deployment['service'] + ' has been updated.')

    def get_deployments(self):
        return [deployment['service'] for deployment in self.__deployments_yaml['deployments']]

    def start(self):
        print('----------------------------------------------------------------------')
        print('Starting...')
        print('----------------------------------------------------------------------')
        self.__check_parameters()
        self.__check_projects_path()
        self.__check_services()
        self.__process_deployments()
        print('Deployment preparation has been completed.')
        print('----------------------------------------------------------------------')


@click.command()
@click.option('--copy-charts', default=True, type=bool)
@click.option('--create-manifests', default=True, type=bool)
@click.option('--env', default='PRODUCTION', type=str) # PRODUCTION, MARKETING
@click.option('--datacenter', default='TCX', type=str) # TCX, EQX
def main(copy_charts, create_manifests, datacenter, env):
    prepare_deploy = PrepareDeploy(copy_charts, create_manifests, datacenter, env)
    prepare_deploy.start()

    ci = ci_yaml(prepare_deploy.get_deployments(), env)
    ci.execute()


if __name__ == '__main__':
    main()
